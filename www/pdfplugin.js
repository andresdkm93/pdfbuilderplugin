var pdfbuild = {

    generate: function(successCallback, errorCallback, name, files, portrait){
        cordova.exec(successCallback,
            errorCallback,
            "PdfBuildPlugin",
            "generate",
            [{
                "name":name,
                "files":files,
                "portrait":portrait
            }]
        );

    }
}

module.exports = pdfbuild;