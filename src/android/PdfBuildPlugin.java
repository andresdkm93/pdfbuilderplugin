package com.andresdkm;

/**
 * Created by andresdkm on 13/11/17.
 */
import android.content.Context;

import org.apache.cordova.CordovaInterface;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PdfBuildPlugin extends CordovaPlugin {

    public static final String GENERATE = "generate";

    public boolean execute(String action, JSONArray jsonArgs,
                           CallbackContext callbackContext) throws JSONException {
        try {
            if (GENERATE.equals(action)) {
                Context context = cordova.getActivity()
                        .getApplicationContext();
                JSONObject args = jsonArgs.getJSONObject(0);
                String name = args.getString("name");
                String files = args.getString("files");
                boolean portrait = args.getBoolean("portrait");
                String[] filesArray=files.split(",");
                PdfBuild builder =new PdfBuild();
                builder.createDocument(name,filesArray,portrait);
                callbackContext.success();
            }
            return true;
        } catch (Exception e) {
            callbackContext.error(e.getMessage());
            return false;
        }
    }
}
